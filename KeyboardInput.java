import java.util.Scanner;

public class KeyboardInput implements StrategyForInput{
    @Override
    public String performInput(){
        System.out.print("Enter the text, please: ");
        Scanner in = new Scanner(System.in);
        String text = in.nextLine();
        return text;
    }
}