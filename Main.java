import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String text;
        System.out.println("Do you want to enter the text by yourself or to read it from the file?\n" +
                "Type 1 or 2.");
        Scanner in = new Scanner(System.in);
        int choice = in.nextInt();
        ContextForInput contextForInput = new ContextForInput();
        if (choice == 1){
            contextForInput.setStrategyForInput(new KeyboardInput());
        }
        else
            if (choice == 2){
                contextForInput.setStrategyForInput(new FileInput());
            }
            else{
                System.out.println("ERROR");
                return;
            }
        text = contextForInput.executeInput();

        System.out.println("Which encryption do you want to use? Type 1, 2 or 3:\n" +
                "1. Permutation;\n" +
                "2. Playfair cypher;\n" +
                "3. Huffman coding.");
        choice = in.nextInt();
        Context context = new Context();
        if (choice == 1){
            context.setStrategy(new Permutation());
        }
        else
            if (choice == 2){
                context.setStrategy(new Playfair());
            }
            else
                if (choice == 3){
                    context.setStrategy(new Huffman());
                }
                else {
                    System.out.println("ERROR");
                    return;
                }

        context.executeStrategy(text);
    }
}
