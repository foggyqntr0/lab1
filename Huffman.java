import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;

public class Huffman implements Strategy{

    //входная строка
    static String text;
    //преобразованный по некоторым правилам текст
    static String compressedText;
    //закодированная последовательность из нулей и единиц
    static String encodedSequence;
    //расшифрованный текст
    static String decodedText;

    //словарь (символ — его код)
    static Map<String, String> dictionary;
    //словарь (код символа — символ) - для расшифровки
    static Map<String, String> reverseDictionary;
    //частотный словарь
    static Map<Character, Integer> dict;
    //очередь с приоритетами для построения дерева
    static PriorityQueue<HuffmanNode> q;
    //корень дерева
    static HuffmanNode root;

    //проход по входной строке и формирование частотного словаря
    public static void formFreqDict() {
        dict = new HashMap<Character, Integer>();
        dictionary = new HashMap<String, String>();
        reverseDictionary = new HashMap<String, String>();
        char cur;
        Character cur0;
        Integer curQ;
        for (int i = 0; i < text.length(); i++) {
            cur = text.charAt(i);
            cur0 = Character.valueOf(cur);
            curQ = 0;
            //если символ уже есть в словаре,
            //увеличиваем значение на 1
            if (dict.get(cur0) != null) {
                curQ = dict.get(cur0);
                curQ++;
                dict.remove(cur0);
                dict.put(cur0, curQ);
            } else
            //иначе - добавляем символ в словарь со значением 1
            {
                curQ = 1;
                dict.put(cur0, curQ);
            }
        }
    }

    //построение дерева
    static void buildTheTree() {
        System.out.println();
        q = new PriorityQueue<HuffmanNode>(dict.size(), new ImplementComparator());
        Iterator<Map.Entry<Character, Integer>> entries = dict.entrySet().iterator();
        //создание очереди с приоритетами
        while (entries.hasNext()) {
            Map.Entry<Character, Integer> entry = entries.next();
            //System.out.println("symbol = " + entry.getKey() + " | freq = " + entry.getValue());
            HuffmanNode hn = new HuffmanNode();

            //формирование узла
            hn.c = entry.getKey().charValue();
            hn.item = entry.getValue().intValue();

            hn.left = null;
            hn.right = null;

            //добавление в очередь
            q.add(hn);
        }

        System.out.println();

        root = null;

        //собственно создание дерева

        //пока очередь не пуста
        while (q.size() > 1) {

            //взять элемент из очереди
            HuffmanNode x = q.peek();
            q.poll();

            HuffmanNode y = q.peek();
            q.poll();

            HuffmanNode f = new HuffmanNode();

            //значение, хранящееся в узле, - сумма частот потомков
            f.item = x.item + y.item;
            f.c = '-';
            f.left = x;
            f.right = y;
            root = f;

            q.add(f);
        }
    }

    //вывод на экран получившегося словаря "символ — код"
    //(рекурсивная процедура)
    public static void printCode(HuffmanNode root, String s) {
        //если потомков нет, закончить формирование кода
        if (root.left == null && root.right == null) {
            //System.out.println(root.c + " |  " + s);
            String key = "";
            key += root.c;
            dictionary.put(key, s);
            reverseDictionary.put(s, key);
            return;
        }
        //иначе продолжить движение по дереву с добавлением 0 или 1 к коду соответственно
        printCode(root.left, s + "0");
        printCode(root.right, s + "1");
    }



    //проверка на то, содержит ли строка цифры
    public static boolean containsNumbers(String txt) {
        for (int i = 0; i < txt.length(); i++) {
            if ((txt.charAt(i) >= '0') && (txt.charAt(i) <= '9')) {
                return true;
            }
        }
        return false;
    }

    //преобразование текста
    public static void compress() {
        text = text.toLowerCase();
        //удаление знаков препинания и других символов
        text = text.replaceAll(",", "%");
        text = text.replaceAll("-", "%");
        text = text.replaceAll(";", "%");
        text = text.replaceAll(":", "%");
        text = text.replaceAll("/", "%");
        text = text.replaceAll("=", "%");
        text = text.replaceAll(" ", "_");
        //если в строке встречаются цифры,
        //заменить буквенную запись цифровой
        if (containsNumbers(text)) {
            text = text.replaceAll("один", "1");
            text = text.replaceAll("дв", "2");
            text = text.replaceAll("три", "3");
            text = text.replaceAll("четыре", "4");
            text = text.replaceAll("пять", "5");
            text = text.replaceAll("шесть", "6");
            text = text.replaceAll("восемь", "8");
            text = text.replaceAll("семь", "7");
            text = text.replaceAll("девять", "9");
            text = text.replaceAll("ноль", "0");
            text = text.replaceAll("one", "1");
            text = text.replaceAll("three", "3");
            text = text.replaceAll("five", "5");
            text = text.replaceAll("four", "4");
            text = text.replaceAll("six", "6");
            text = text.replaceAll("seven", "7");
            text = text.replaceAll("eight", "8");
            text = text.replaceAll("nine", "9");
            text = text.replaceAll("ate", "8");
            text = text.replaceAll("to", "2");
            text = text.replaceAll("for", "4");
        }
        //заменить глухие согласные на звонкие
        //и фонетически сходные гласные
        text = text.replaceAll("ё", "е");
        text = text.replaceAll("й", "и");
        text = text.replaceAll("к", "г");
        text = text.replaceAll("п", "б");
        text = text.replaceAll("с", "з");
        text = text.replaceAll("т", "д");
        text = text.replaceAll("ф", "в");
        text = text.replaceAll("щ", "ш");
        text = text.replaceAll("ц", "ч");
        text = text.replaceAll("ь", "'");
        text = text.replaceAll("ы", "%");
        text = text.replaceAll("ъ", "'");
        text = text.replaceAll("э", "е");
        text = text.replaceAll("ю", "у");
        text = text.replaceAll("я", "а");
        //преобразования для латинских символов
        text = text.replaceAll("ough", "o");
        text = text.replaceAll("you", "u");
        text = text.replaceAll("ou", "u");
        text = text.replaceAll("th", "s");
        text = text.replaceAll("ng", "n");
        text = text.replaceAll("ght", "t");
        text = text.replaceAll("gh", "g");
        text = text.replaceAll("wh", "v");
        text = text.replaceAll("w", "v");
        text = text.replaceAll("z", "s");
        text = text.replaceAll("y", "i");
        text = text.replaceAll("j", "i");
        text = text.replaceAll("q", "k");
        text = text.replaceAll("x", "k");
        compressedText = "";

        //удаление служебных символов
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) != '%') {
                compressedText += text.charAt(i);
            }
        }

        int i = 0;
        String toReplace = "";
        String changing = "";

        //замена идущих подряд одинаковых символов одним символом
        while (i < compressedText.length() - 1) {
            if (compressedText.charAt(i) == compressedText.charAt(i + 1)) {
                toReplace += compressedText.charAt(i);
                toReplace += compressedText.charAt(i);
                changing += compressedText.charAt(i);
                compressedText = compressedText.replaceAll(toReplace, changing);
                toReplace = "";
                changing = "";
            } else
                i++;
        }
        //System.out.println(compressedText);
    }

    //проход по входной строке
    //и формирование закодированной последовательности
    //в соответствии со словарём
    public static void formTheCode() {
        String current;
        String currentCode;
        encodedSequence = "";
        for (int i = 0; i < compressedText.length(); i++) {
            current = "";
            current += compressedText.charAt(i);
            currentCode = dictionary.get(current);
            encodedSequence += currentCode;
        }
    }

    @Override
    public void start(String text) {
        this.text = text;
        this.compress();
        if (this.compressedText.length() < 2){
            //если длина текста после компрессии меньше, чем 2,
            //дерево построить будет невозможно
            System.out.println("Something went wrong.");
        }
        else {
            //создать частотный словарь
            this.formFreqDict();
            //построить дерево
            this.buildTheTree();
            this.printCode(root, "");
            //создать кодовый словарь
            this.formTheCode();
            System.out.println(encodedSequence);
            //расшифровать получившуюся последовательность
            //decoder();
            //System.out.println("After decoding: " + decodedText);
    }

    }
}