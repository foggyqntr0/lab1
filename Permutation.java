import java.util.Random;

public class Permutation implements Strategy{
    private String text;
    private String newText;
    private int rows;
    private int columns;
    private int[] rowsSeq;
    private int[] columnsSeq;
    private int length;

    private boolean checkTheString(){
        String empty = "";
        if (this.text.length() == empty.length()){
            return false;
        }
        else
            return true;
    }

    private int checkAndChange(){
        if (this.text.length() < this.rows * this.columns){
            char[] additional = new char[this.rows * this.columns - this.text.length()];
            for (int i = 0; i < this.rows * this.columns - this.text.length() - 1; i++)
                additional[i] = '=';
            String additionalString = "";
            additionalString = String.copyValueOf(additional);
            this.text = this.text.concat(additionalString);
            return 1;
        }
        return 1;
    }

    public int getRandom(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }

    public int inputWithGeneration(){
        if (!checkTheString())
            return 1;

        this.length = text.length();

        double lengthSpecific = Math.sqrt(length);

        this.rows = (int) Math.floor(lengthSpecific);

        this.columns = 1;

        while (this.rows * this.columns < this.length)
            this.columns++;

        if (checkAndChange() == -1)
            return 1;

        int times = (int) (Math.random()*100);

        this.rowsSeq = new int[this.rows];
        for (int i = 0; i < rows; i++)
            rowsSeq[i] = i+1;
        this.columnsSeq = new int[this.columns];
        for (int i = 0; i < columns; i++)
            columnsSeq[i] = i+1;

        for (int t = 0; t < 10; t++){
            int random1 = getRandom(0, rows);
            int random2 = getRandom(0, rows);
            int x;
            x = rowsSeq[random1];
            rowsSeq[random1] = rowsSeq[random2];
            rowsSeq[random2] = x;
        }

        times = (int) Math.random()*100;

        for (int t = 0; t < 10; t++){
            int random1 = getRandom(0, columns);
            int random2 = getRandom(0, columns);
            int x;
            x = columnsSeq[random1];
            columnsSeq[random1] = columnsSeq[random2];
            columnsSeq[random2] = x;
        }
        //System.out.println("The cipher is:");
        //System.out.println(this.rows);

        for (int i = 0; i < rows; i++){
            //System.out.println(rowsSeq[i]);
            //System.out.print(' ');
        }
        //System.out.println();
        //System.out.println(this.columns);

        for (int i = 0; i < columns; i++) {
            //System.out.println(columnsSeq[i]);
            //System.out.print(' ');
        }
        //System.out.println();
        return 0;
    }

    private String cipher() {
        char[][] aux = new char[rows][columns];
        length = text.length();
        int i = 0;
        while (i < rows * columns) {
            for (int r = 0; r < rows; r++)
                for (int c = 0; c < columns; c++) {
                    aux[rowsSeq[r]-1][c] = text.charAt(i);
                    i++;
                }
        }
        char[] nText = new char[rows * columns];
        int k = 0;
        for (int c = 0; c < columns; c++)
            for (i = 0; i < rows; i++) {
                nText[k] = aux[i][columnsSeq[c]-1];
                k++;
            }
        newText = String.copyValueOf(nText);
        System.out.println("The encrypted text is: ");
        System.out.println(newText);
        return newText;
    }

    @Override
    public void start(String text){
        this.text = text;
        this.inputWithGeneration();
        this.cipher();
    }
}