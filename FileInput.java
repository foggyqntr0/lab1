import java.io.*;

public class FileInput implements StrategyForInput{
    public String nullstr = "";
    @Override
    public String performInput(){
        try {
            File file = new File("input.txt");
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine();
            return line;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nullstr;
    }
}
