public interface StrategyForInput {
    String performInput();
}
