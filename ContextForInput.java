public class ContextForInput {
    private StrategyForInput strategyForInput;

    public ContextForInput() {
    }

    public void setStrategyForInput(StrategyForInput strategyForInput) {
        this.strategyForInput = strategyForInput;
    }

    public String executeInput() {
       return strategyForInput.performInput();
    }
}
