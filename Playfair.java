import java.util.Scanner;

public class Playfair implements Strategy{
    private String codeword;
    private String codewordEdited;
    private String text;
    private String enABC = "ABCDEFGHIKLMNOPQRSTUVWXYZ";
    private String rusABC = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";
    private String ABC;
    private String newText;
    private int rows;
    private int columns;
    private char[][] matrix;
    private int x1, x2, y1, y2;
    private char specialSymbol;

    private int processTheInput() {
        this.codewordEdited = "";
        this.codeword = this.codeword.toUpperCase();
        this.text = this.text.toUpperCase();
        this.text = this.text.replaceAll(" ", "X");

        if (!((isOkEnglish(this.codeword) && containsEn(this.codeword)) || (isOkRussian(this.codeword) && containsRus(this.codeword))))
        {
            System.out.println(isOkEnglish(this.codeword));
            System.out.println(containsEn(this.codeword));
            System.out.println(isOkRussian(this.codeword));
            System.out.println(containsRus(this.codeword));
            System.out.println("SHIT");
            return -1;
        }

        //if (isOkRussian(this.codeword) != isOkRussian(this.text) || isOkEnglish(this.codeword) != isOkEnglish(this.text))
        //    return 1;

        if (codeword.charAt(0) >= 'A' && codeword.charAt(0) <= 'Z') {
            this.ABC = enABC;
            rows = 5;
            columns = 5;
            specialSymbol = 'X';
            this.text = this.text.replaceAll("J", "I");
        }
        else if (codeword.charAt(0) >= 'А' && codeword.charAt(0) <= 'Я'){
            this.ABC = rusABC;
            rows = 4;
            columns = 8;
            specialSymbol = 'Ъ';
            this.text = this.text.replaceAll("Ё", "Е");
        }
        //else {
        //???
        //}

        this.matrix = new char[rows][columns];
        for (int i = 0; i < this.codeword.length(); i++) {
            if (this.codeword.indexOf(this.codeword.charAt(i)) == i) {
                this.codewordEdited += this.codeword.charAt(i);
            }
            this.ABC = this.ABC.replace(this.codeword.charAt(i), '_');
        }

        this.ABC = this.ABC.replaceAll("_", "");

        //System.out.println(ABC);

        int i = 0, j = 0, k = 0;
        while (k < this.codewordEdited.length()) {
            this.matrix[i][j] = this.codewordEdited.charAt(k);
            k++;
            //System.out.println(this.matrix[i][j]);
            j++;
            if (j == columns){
                j = 0;
                i++;
            }
        }

        //System.out.println(i);
        //System.out.println(j);
        //System.out.println(ABC);

        k = 0;

        while (k < ABC.length()) {
            this.matrix[i][j] = this.ABC.charAt(k);
            //System.out.print(this.matrix[i][j]);
            j++;
            if (j == columns) {
                j = 0;
                i++;
            }
            k++;
        }

        for (i = 0; i < rows; i++) {
            for (j = 0; j < columns; j++) {
                //System.out.print(this.matrix[i][j]);
                //System.out.print(" ");
            }
            //System.out.println();
        }

        for (i = 0; i < this.text.length(); i++)
            if ( !( (this.text.charAt(i) >= 'A' && this.text.charAt(i) <= 'Z') || (this.text.charAt(i) >= 'А' && this.text.charAt(i) <= 'Я')))
                this.text = this.text.replace(this.text.charAt(i), ' ');

        this.text = this.text.replaceAll(" ", "");

        if (this.text.length() % 2 != 0)
            this.text += specialSymbol;

        //System.out.println(this.text);

        return 0;

    }

    private boolean isOkRussian(String checking){
        for (int i = 0; i < checking.length(); i++)
            if (checking.charAt(i) < 'А' || checking.charAt(i) > 'Я')
                return false;
        return true;
    }

    private boolean isOkEnglish(String checking){
        for (int i = 0; i < checking.length(); i++)
            if (checking.charAt(i) < 'A' || checking.charAt(i) > 'Z')
                return false;
        return true;
    }

    private boolean containsRus(String checking){
        for (int i = 0; i < checking.length(); i++)
            if (checking.charAt(i) == 'Ё')
                return false;
        return true;
    }

    private boolean containsEn(String checking){
        for (int i = 0; i < checking.length(); i++)
            if (checking.charAt(i) == 'J')
                return false;
        return true;
    }

    private void findPositions(char sym1, char sym2){
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < columns; j++){
                if (this.matrix[i][j] == sym1){
                    this.x1 = i;
                    this.y1 = j;
                }
                if (this.matrix[i][j] == sym2){
                    this.x2 = i;
                    this.y2 = j;
                }
            }
    }

    private void encrypt(){
        newText = "";
        int temp, b = 0;
        char B1, B2, newB1, newB2;
        while (b < this.text.length()){
            if (b == this.text.length() - 1)
            {
                B1 = this.text.charAt(b);
                B2 = specialSymbol;
                b++;
            }
            else {
                B1 = this.text.charAt(b);
                B2 = this.text.charAt(b + 1);
            }

            if (B1 == B2){
                B2 = specialSymbol;
                b++;
            }
            else{
                b += 2;
            }

            findPositions(B1, B2);
            if (x1 == x2){
                if (y1 + 1 < columns) y1++;
                else y1 = 0;
                if (y2 + 1 < columns) y2++;
                else y2 = 0;
            }
            else if (y1 == y2){
                if (x1 + 1 < rows) x1++;
                else x1 = 0;
                if (x2 + 1 < rows) x2++;
                else x2 = 0;
            }
            else {
                temp = y1;
                y1 = y2;
                y2 = temp;
            }
            newB1 = this.matrix[x1][y1];
            newB2 = this.matrix[x2][y2];
            this.newText += newB1;
            this.newText += newB2;
        }
        System.out.println(this.newText);
    }

    public void start(String text){
        this.text = text;
        this.codeword = text.substring(this.text.length()/2, this.text.length());
        if (this.processTheInput() != 0)
            System.out.println("ERROR");
        else
            this.encrypt();
    }
}
