public interface Strategy {
    void start(String text);
}
