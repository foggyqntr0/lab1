//класс, описывающий узел дерева
class HuffmanNode {
    //частота
    int item;
    //символ
    char c;
    //левый потомок
    HuffmanNode left;
    //правый потомок
    HuffmanNode right;
}